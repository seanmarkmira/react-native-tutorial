import React, {useState} from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button } from 'react-native';

export default function App() {
  const [name, setName] = useState('Water');
  const [session, setSession] = useState({number:6, title: 'state'});
  const [current, setCurrent] = useState(true);

  const onClickHandler = () => {
    setName("Laundry");
    setSession({number: 7, title: 'Style'})
    setCurrent(false)
  }
  
  return (
    <View style={styles.body}>
      <Text style={styles.text}>You may need {name}</Text>
      <Text style={styles.text}>This is the number: {session.number} and title: {session.title}</Text>
      <Text style={styles.text}>{current ? 'current session': 'next session'}</Text>
      <Button title='I need laundry' onPress={onClickHandler}></Button>
    </View>
  );
}

const styles = StyleSheet.create({
  body:{
    flex: 1,
    backgroundColor: '#0000ff',
    alignItems: 'center',
    justifyContent:'center'
  },
  text:{
    color: '#ffffff',
    fontSize: 20,
    fontStyle: 'italic',
  },
});
